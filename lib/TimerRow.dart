import 'package:flutter/material.dart';

import 'TimerModel.dart';
import 'utils.dart';


class TimerRow extends StatelessWidget {
  final TimerModel timer;
  final Function onTimerStopped;
  final Function onTimerStarted;

  TimerRow({
    required this.timer,
    required this.onTimerStarted,
    required this.onTimerStopped,

    Key? key,
  }) : super(key: key);

  Widget get timerDuration {
    if (timer.dateStarted != null) {
      int remains = timer.remainingSec;
      if (remains > 0) {
        return Text(formatSeconds(remains));
      } else {
        return Text(formatSeconds(remains), style: TextStyle(color: Colors.redAccent));
      }
    } else {
      return Text(formatSeconds(timer.durationSec));
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: timer.isRunning ?
        timer.remainingSec > 0 ?
          CircularProgressIndicator(
            value: (timer.remainingSec / timer.durationSec),
            backgroundColor: Colors.black12,
          )
          :
          CircleAvatar(child: Icon(Icons.warning_amber, color: Colors.white), backgroundColor: Colors.redAccent,)
        :
          CircularProgressIndicator(
            value: 0.9,
            backgroundColor: Colors.black12,
            valueColor: AlwaysStoppedAnimation<Color>(timer.color),
          )
      ,
      title: Text(timer.title),
      subtitle: timerDuration,
      trailing: timer.isRunning ?
        IconButton(
          icon: Icon(Icons.stop_outlined),
          onPressed: () => onTimerStopped(timer),
        )
        :
        IconButton(
          icon: Icon(Icons.play_arrow_outlined),
          onPressed: () => onTimerStarted(timer),
        )
      ,
      onTap: () => onTimerStarted(timer),
      // height: 50,
      // color: Colors.amber[600],
      // child: Center(child: Text(timer.title)),
    );
  }
}
