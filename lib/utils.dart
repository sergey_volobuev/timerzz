

formatSeconds(int totalSeconds) {
  
  if (totalSeconds < 0) {
    return '${totalSeconds} seconds';
  } else if (totalSeconds == 0) {
    return 'NOW';
  }

  int minutes = totalSeconds ~/ 60;
  int seconds = totalSeconds % 60;
  if (minutes != 0) {
    if (seconds == 0) {
      return '${minutes} minutes';
    }
    return '${minutes}m ${seconds}s';
  } else {
    return '${seconds} seconds';
  }
}