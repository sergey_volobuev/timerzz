
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';


class TimerModel {
  final String title;
  final int durationSec;
  final int playTickAt;
  final Color color;
  
  DateTime? dateStarted;
  bool tickPlayed = false;
  bool bellPlayed = false;

  TimerModel({
    required this.title,
    required this.durationSec,
    required this.playTickAt,
    required this.color,
  });

  bool get isRunning {
    return dateStarted != null;
  }

  int get remainingSec {
    if (dateStarted == null) { return 0; }
    var now = DateTime.now();
    var elapsed = now.difference(dateStarted!).inSeconds;
    var remains = durationSec - elapsed;
    return remains;
  }

  start() {
    dateStarted = DateTime.now();
    tickPlayed = false;
    bellPlayed = false;
  } 

  stop() {
    dateStarted = null;
  }

  periodic() {
    if (!tickPlayed && playTickAt > 0 && remainingSec < playTickAt) {
      tickPlayed = true;
      playTick();
    }

    if (!bellPlayed && remainingSec < 4) {
      bellPlayed = true;
      playBell();
    }

  }


  Future<AudioPlayer> playTick() async {
    AudioCache cache = new AudioCache();
    // At the next line, DO NOT pass the entire reference such as assets/yes.mp3. This will not work.
    // Just pass the file name only.
    return await cache.play("tick.wav"); 
  }

  Future<AudioPlayer> playBell() async {
      AudioCache cache = new AudioCache();
    //At the next line, DO NOT pass the entire reference such as assets/yes.mp3. This will not work.
    //Just pass the file name only.
      return await cache.play("bell.wav"); 
  }

}