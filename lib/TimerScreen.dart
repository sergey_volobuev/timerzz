import 'package:flutter/material.dart';

import 'TimerModel.dart';
import 'utils.dart';


class TimerScreen extends StatelessWidget {
  final TimerModel timer;
  final TimerModel nextTimer;
  final Function onTimerStopped;
  final Function onNextTimer;

  TimerScreen({
    required this.timer,
    required this.nextTimer,
    required this.onTimerStopped,
    required this.onNextTimer,

    Key? key,
  }) : super(key: key);

  Widget get timerDuration {
    String label = '';

    if (timer.dateStarted != null) {
      int remains = timer.remainingSec;
      if (remains > 0) {
        label = formatSeconds(remains);
      } else {
        label = formatSeconds(remains);
      }
    } else {
      label = formatSeconds(timer.durationSec);
    }
    return Text(label, style: TextStyle(fontSize: 24));
  }

  Animation<Color> get valueColor {
    int remaining = timer.remainingSec;
    if (remaining < 0) {
      return AlwaysStoppedAnimation<Color>(Colors.redAccent);
    }

    if (remaining < timer.playTickAt) {
      return AlwaysStoppedAnimation<Color>(Colors.amberAccent);
    }

    return AlwaysStoppedAnimation<Color>(timer.color);
  }

  Widget get progress {
      return timer.isRunning ?

          Container(
            width: 250,
            height: 250,
            child: CircularProgressIndicator(
              value: timer.remainingSec > 0 ? (timer.remainingSec / timer.durationSec) : 1.0,
              backgroundColor: Colors.black12,
              valueColor: valueColor,
              strokeWidth: 16,
            ),
          )
        :
          CircularProgressIndicator(
            value: 0,
            backgroundColor: Colors.black12,
          )
      ;

  }

  Widget get titleWidget {
    return Padding(
      padding: const EdgeInsets.only(top: 64.0),
      child: Text(timer.title, textAlign: TextAlign.center, style: TextStyle(fontSize: 24)),
    );
  }

  @override
  Widget build(BuildContext context) {

    if (!timer.isRunning) { return Container(width:0, height: 0); }
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
      titleWidget,
      Expanded(child: Stack(
        children: [
          Center(child: progress),
          Center(child: timerDuration),
        ],
      )),
      Padding(
        padding: const EdgeInsets.only(bottom:32.0, left: 16, right: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
              OutlinedButton.icon(
                icon: Icon(Icons.stop_outlined),
                label: Text('Stop'),
                onPressed: () => onTimerStopped(timer),
              ),
              OutlinedButton.icon(
                icon: Text('Next (${nextTimer.title})'),
                label: Icon(Icons.skip_next_outlined), 
                onPressed: () => onNextTimer(timer),
                // style: TextButton.styleFrom(primary: nextTimer.color),
              ),
          ],
        ),
      ),
      ],
    );
  }

}
