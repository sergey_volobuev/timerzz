import 'package:flutter/material.dart';

import 'TimerModel.dart';
import 'TimerRow.dart';

class TimerList extends StatelessWidget {

  final List<TimerModel> timers;
  final Function onTimerStopped;
  final Function onTimerStarted;


  const TimerList({
    required this.timers,
    required this.onTimerStopped,
    required this.onTimerStarted,
    Key? key,
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemCount: timers.length,
      separatorBuilder: (BuildContext context, int index) => Divider(),
      itemBuilder: (BuildContext context, int index) => TimerRow(
        timer: timers[index],
        onTimerStopped: onTimerStopped,
        onTimerStarted: onTimerStarted,
      )
    );
  }
}
