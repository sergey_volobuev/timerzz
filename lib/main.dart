import 'dart:async';

import 'package:flutter/material.dart';

import 'TimerList.dart';
import 'TimerModel.dart';
import 'TimerScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Exam timer',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Exam timer'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  List<TimerModel> timers = [
    TimerModel(title: 'Examination', durationSec: 12 * 60, playTickAt: 60, color: Colors.blue),
    TimerModel(title: 'Questions', durationSec: 2 * 60, playTickAt: 15, color: Colors.green),
    TimerModel(title: 'Marking', durationSec: 1 * 60, playTickAt: 0, color: Colors.pink.shade200),
  ];

  TimerModel? currentTimer;

  DateTime now = DateTime.now();

  late final Timer timer;

  _MyHomePageState() {
    // super();
    timer = new Timer.periodic(new Duration(seconds: 1), (timer) {
      debugPrint(timer.tick.toString());
      if (currentTimer != null) {
        setState(() {
          currentTimer?.periodic();
          now = DateTime.now();
        });
      }
    });
  }

  handleStopTimer(TimerModel t) {
    setState(() {
      t.stop();
      currentTimer = null;
    });
  }

  handleNextTimer(TimerModel thisTimer) {
    setState(() {
      thisTimer.stop();
      TimerModel nextTimer = getNextTimer(thisTimer);
      nextTimer.start();
      currentTimer = nextTimer;
    });
  }

  handleStartTimer(TimerModel t) {
    setState(() {
      t.start();
      currentTimer = t;
    });
  }

  getNextTimer(TimerModel thisTimer) {
    for (int i = 0; i < timers.length; i++) {
      if (timers[i] == thisTimer) {
        if (i < timers.length - 1) {
          return timers[i + 1];
        } else {
          return timers[0];
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: currentTimer == null ? AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ) : null,
      body: currentTimer != null ? 
        TimerScreen(
          timer: currentTimer!,
          nextTimer: getNextTimer(currentTimer!),
          onTimerStopped: handleStopTimer,
          onNextTimer: handleNextTimer,
        )
        :
        TimerList(
          timers: timers,
          onTimerStopped: handleStopTimer,
          onTimerStarted: handleStartTimer,
        ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}

